package grpcserver

import (
	"protobuffer/cmd"
	"protobuffer/datalayer"
	"context"
)

type GrpcServer struct{
	dbHandler *datalayer.SQLHandler
	cmd.UnimplementedPersonServiceServer
}
func NewGrpcServer(connString string)(*GrpcServer,error){
	db, err := datalayer.CreateDBConnection(connString)
	if err != nil {
		return nil, err
	}
	return &GrpcServer{
		dbHandler: db,
	}, err
}
func (server *GrpcServer) GetPerson(ctx context.Context, r *cmd.Request) (*cmd.Person, error) {
	person, err := server.dbHandler.GetPersonByName(r.GetName())
	if err != nil {
		return nil, err
	}
	return convertToGrpcPerson(person), nil
}

func (server *GrpcServer) GetPeople(r *cmd.Request, stream cmd.PersonService_GetPeopleServer) error {
	people, err := server.dbHandler.GetPeople()
	if err != nil {
		return err
	}
	for _, person := range people {
		grpcPerson := convertToGrpcPerson(person)
		err := stream.Send(grpcPerson)
		if err != nil {
			return err
		}
	}
	return nil
}
func convertToGrpcPerson(person datalayer.Person) *cmd.Person {
	return &cmd.Person{
		Id:     int32(person.ID),
		Name:   person.Name,
		Family: person.Family,
	}
}
