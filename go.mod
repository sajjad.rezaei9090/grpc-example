module protobuffer

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	google.golang.org/grpc v1.36.1
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0 // indirect
	google.golang.org/protobuf v1.26.0
)
